#!/usr/bin/env python3
from z3 import Real, SolverFor, sat, If
import sys
import matplotlib.pyplot as plt


def main(vmax=50, vmin=0, dstart=1e-6, mindecel=-4.5, maxdecel=-6.44,
         minreactiontime=1.2, maxreactiontime=1.5):
    # Check that mindecel <= maxdecel
    if mindecel < maxdecel:
        print('Min decel should be greater than Max decel, min: %s, max: %s' %
              (mindecel, maxdecel))
        sys.exit(1)
    # Check min and max reaction times
    if minreactiontime > maxreactiontime:
        print('Min reaction time should be less than Max reaction time')
        sys.exit(1)
    # Check vmin and vmax
    if vmin > vmax:
        print('Min velocity should be less than or equal to Max velocity')
        sys.exit(1)

    # tl = Real('tl')
    tf = Real('tf')
    al = Real('al')
    af = Real('af')
    vl = Real('vl')
    vf = Real('vf')
    t = Real('t')

    vmax = vmax*(1000/3600)          # kph → mps
    vmin = vmin*(1000/3600)          # kph → mps

    # The initial spacing between two cars, includes everything
    dsafe = Real('dsafe')
    solver = SolverFor(logic='QF_NRA')

    # Because accell is negative in this encoding unlike the paper
    solver.add(al <= mindecel, al >= maxdecel)
    solver.add(af <= mindecel, af >= maxdecel)

    # This is the bounds for time, leader and follower come to a stop.
    # Guarantee that t - (vf/af) > (-vl/al)
    solver.add((t - (vf/af)) >= (-(vl/al)))
    # solver.add(tl >= 0, tl <= -(vl/al))
    solver.add(tf >= 0, tf <= t - (vf/af))

    solver.add(dsafe >= dstart)
    if vmin == 0:
        solver.add(vl > vmin, vl <= vmax)
        solver.add(vf > vmin, vf <= vmax)
    else:
        solver.add(vl >= vmin, vl <= vmax)
        solver.add(vf >= vmin, vf <= vmax)

    # The below 4 hypothesis are included already by default.
    # XXX: We need to prove the above statement.

    # Hypothesis - 1 (vf >= vl) worst case
    # solver.add(vf >= vl)
    # Hypothesis - 2 (af < al) worst case
    # solver.add(af <= al)

    # Hypothesis - 1' (best case)
    # solver.add(vf <= vl)
    # Hypothesis - 2' (best case)
    # solver.add(af >= al)

    solver.add(t >= minreactiontime, t <= maxreactiontime)

    # Safety distance with driver reaction delay (reaction delay should be -)
    L = 5
    xl = If((tf > (-(vl/al))),
            ((al*(-(vl/al))*(-(vl/al)))/2 + (vl * (-(vl/al)))),
            (al*(tf*tf)/2 + (vl*tf)))
    xf = If((tf >= t), ((af*((tf-t)*(tf-t))/2) + (vf*(tf-t)) + (vf * t)),
            (vf*tf))
    eq = (xl - xf - L + dsafe)

    # It can happen before they both come to a stop!
    solver.add(eq <= 0)          # Can collision happen?
    return (solver.check(), dstart)


# Binary search for an optimal "d" for improved throughput
def search(ub, lb, vmax, vmin, mindecel, maxdecel,
           minreactiontime, maxreactiontime, epsilon=1e-6):
    if (ub - lb <= epsilon):
        return lb
    else:
        half = lb + ((ub - lb)/2.0)
        (ret, _) = main(vmax, vmin, half, mindecel, maxdecel,
                        minreactiontime, maxreactiontime)
        if ret == sat:
            return search(ub, half, vmax, vmin, mindecel, maxdecel,
                          minreactiontime, maxreactiontime)
        else:
            return search(half, lb, vmax, vmin, mindecel, maxdecel,
                          minreactiontime, maxreactiontime)


def call_main(vmin=0, vmax=50, bmin=-1.0, bmax=-5.0):
    d = dprev = 1e-6               # in meters
    mindecel = bmin                # The min deceleration of cars (m/s²)
    maxdecel = bmax               # The max deceleration of cars (m/s²)
    minreactiontime = 1.2          # in s
    maxreactiontime = 1.2          # in s
    (ret, d) = main(vmax=vmax, vmin=vmin,
                    mindecel=mindecel, maxdecel=maxdecel,
                    minreactiontime=minreactiontime,
                    maxreactiontime=maxreactiontime)
    # Now check if the model is sat or unsat
    while (ret == sat):
        dprev = d
        d *= 2
        (ret, d) = main(vmax=vmax, vmin=vmin,
                        mindecel=mindecel, maxdecel=maxdecel,
                        minreactiontime=minreactiontime,
                        maxreactiontime=maxreactiontime,
                        dstart=d)
    else:
        # Now do a binary search between d and dprev
        d = search(d, dprev, vmax, vmin, mindecel, maxdecel,
                   minreactiontime, maxreactiontime)
        return (d, mindecel, maxdecel, minreactiontime, maxreactiontime)


def compute_flow(v, d):
    return (1000.0/(d)) * v


def binary_search(ub, lb, epsilon=1e-6):
    if (ub - lb <= epsilon):
        (d, mind, _, minrt, _) = call_main(vmax=lb)
        flow = compute_flow(lb, d)
        return (lb, flow)
    else:
        half = lb + ((ub - lb)/2.0)
        (d, mind, _, minrt, _) = call_main(vmax=half)
        flow = compute_flow(half, d)
        (d, mind, _, minrt, _) = call_main(vmax=lb)
        lflow = compute_flow(lb, d)
        if (flow > lflow):
            return binary_search(ub, half)
        else:
            return binary_search(half, lb)


def optimal_velocity_flow():
    pre_flow = 0
    pre_v = 0
    v = 1e-6
    (d, mind, maxd, minrt, maxrt) = call_main(vmax=v)
    flow = compute_flow(v, d)
    while (flow > pre_flow):
        pre_v = v
        pre_flow = flow
        v *= 2
        (d, mind, maxd, minrt, maxrt) = call_main(vmax=v)
        flow = compute_flow(v, d)
    else:
        return binary_search(v, pre_v)


if __name__ == '__main__':
    plt.style.use('ggplot')

    # Get the optimal (max) flow in the best case scenario
    # (vbest, bflow) = optimal_velocity_flow()

    # printing the graph of flow vs. v
    START = 0
    MAX = 51
    STEP = 2
    ret1, ret2 = [], []
    for v in range(START, MAX, STEP):
        (d, mind, maxd, minrt, maxrt) = call_main(vmin=v, vmax=v, bmin=-5,
                                                  bmax=-6)
        # The flow
        bf = compute_flow(v, d)
        ret1.append(bf)
        print('(v, d, flow): %0.2f, %0.2f, %0.2f' % (v, d, bf))

    plt.plot(list(range(START, MAX, STEP)), ret1)
    plt.xlabel('Upper bound on initial LV and FV veclocity ranges (km/h)')
    plt.ylabel('Flow (vh/hr/lane)')
    # plt.title('Best/Worst flow ' + 'vs. velocity: best (velocity, flow):'
    #           + '%0.2f km/h, %0.2f vh/hr/lane' % (vbest, bflow))
    # plt.savefig('./trc_paper/figs/flowvar.pdf', bbox_inches='tight')
    plt.show()
