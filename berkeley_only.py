import matplotlib.pyplot as plt
import numpy as np


def berkeley(t_r):
    d = 5
    init = (0.5 * (((vf*vf)/af) - ((vl*vl)/al))) + (vf * t_r) + d
    assert(t_r - (vf/af) > (-vl/al))
    ret = []
    for i in t:
        xl = ((al*((-vl/al)*(-vl/al))/2 + (vl*(-vl/al)))
              if i > (-vl/al) else (al*(i*i)/2 + (vl*i)))
        xf = (((af*((i-t_r)*(i-t_r))/2) + (vf*(i-t_r))
               + (vf * t_r))
              if i > t_r else (vf * i))
        crash = xl - xf + init
        ret.append(crash)
    return ret


def mine(t_r):
    init = {'0.25': 8.54, '3.0': 47.5}
    assert(t_r - (vf/af) > (-vl/al))
    ret = []
    for i in t:
        xl = ((al*((-vl/al)*(-vl/al))/2 + (vl*(-vl/al)))
              if i > (-vl/al) else (al*(i*i)/2 + (vl*i)))
        xf = (((af*((i-t_r)*(i-t_r))/2) + (vf*(i-t_r))
               + (vf * t_r))
              if i > t_r else (vf * i))
        crash = xl - xf + init[str(t_r)]
        ret.append(crash)
    return ret


t_r = [0.25, 3.0]
al = -5
af = -5.0                       # Same velocity but follower stops slower
vf = (51.0 * (1000/3600))  # km/h → m/s
vl = (51 * (1000/3600))  # km/h → m/s


if __name__ == '__main__':
    plt.style.use('ggplot')
    dsafe = dict()
    for tr in t_r:
        t = list(np.arange(0, (tr - (vf/af)), 1e-2))
        dsafe['collision'] = [0]*len(t)
        dsafe['Berkeley_crash_dist_%s' % tr] = berkeley(tr)
        dsafe['Proposed_crash_dist_%s' % tr] = mine(tr)
        plt.plot(t, 'Berkeley_crash_dist_%s' % tr, data=dsafe, linestyle=':')
        plt.plot(t, 'Proposed_crash_dist_%s' % tr, data=dsafe, linestyle='-.')
    plt.plot(t, 'collision', data=dsafe)
    plt.xlabel('Time (sec)')
    plt.ylabel('Distance between LV and FV (m)')
    plt.legend(['Berkeley_vehicle_dist_tr_0.25',
                'Proposed_vehicle_dist_tr_0.25',
                'Berkeley_vehicle_dist_tr_3.0',
                'Proposed_vehicle_dist_tr_3.0',
                'Collision line'])
    plt.savefig('./trc_paper/figs/resultdifftr.pdf', bbox_inches='tight')
    plt.show()
