from z3 import Real, SolverFor, And


def main():
    tl = Real('tl')
    tf = Real('tf')
    al = Real('al')
    af = Real('af')
    vl = Real('vl')
    vf = Real('vf')
    # The initial spacing between two cars, includes everything
    d = Real('d')
    solver = SolverFor(logic='QF_NRA')
    solver.add(And(al < 0, al >= -8))
    solver.add(And(af < 0, af >= -8))

    # Mazda requirement
    # solver.add(And(al == -8, af == -6))

    # Fixing mazda requirement
    solver.add(And(af == al))

    # This is the bounds for time, leader and follower come to a stop.
    solver.add(And(tl >= 0, tl <= -(vl/al)))
    solver.add(And(tf >= 0, tf <= -(vf/af)))

    # Just at the stop time If you comment this line, then you can find
    # the case where collision happens before the stop time of both
    # vehicles.
    solver.add(And(tl == (-(vl/al)), tf == (-(vf/af))))

    # r_min  == 5 meters
    solver.add(And(d >= 0, d == 5))
    solver.add(And((vl > 0, vl <= 250/9)))  # 100km/hr max
    solver.add(And((vf > 0, vf <= 250/9)))  # 100km/hr max

    # The case when leader and follower are almost going at the same
    # speed. -- this is an assumption for MAZDA and Berkeley algorithms.
    solver.add(vl == vf)

    # Safety distance with driver reaction delay -- Mazda
    eq = (al*(tl*tl)/2) - (af*(tf*tf)/2) + (vl*tl) - (vf*tf) + d + (vf * 0.1)
    + (0.6 * ((al*tl + vl) - (af*tf + vf)))

    # It can happen before they both come to a stop!
    solver.add(eq < 0)          # Can collision happen?
    print(solver.check())
    print(solver.to_smt2())
    print(solver.model())


if __name__ == '__main__':
    main()
