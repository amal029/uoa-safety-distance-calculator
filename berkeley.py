import matplotlib.pyplot as plt
import numpy as np


def modelmovement(vl, al, vf, af, t_r, init):
    t = list(np.arange(0, (t_r - (vf/af)), 1e-2))
    ret = []
    for i in t:
        xl = ((al*((-vl/al)*(-vl/al))/2 + (vl*(-vl/al)))
              if i > (-vl/al) else (al*(i*i)/2 + (vl*i)))
        xf = (((af*((i-t_r)*(i-t_r))/2) + (vf*(i-t_r))
               + (vf * t_r))
              if i > t_r else (vf * i))
        crash = xl - xf + init
        ret.append(crash)
    return ret


def berkeley(mvf, mvl, t_r, al, af):
    t_r = t_r
    d = 5
    vf = (mvf * (1000/3600))  # km/h → m/s
    vl = (mvl * (1000/3600))  # km/h → m/s
    al = al
    af = af
    init = (0.5 * (((vf*vf)/abs(af)) - ((vl*vl)/abs(al)))) + (vf * t_r) + d
    assert(t_r - (vf/af) > (-vl/al))
    return modelmovement(vl, al, vf, af, t_r, init)


def sda(mvf, mvl, t_r, al, af):
    t_r = t_r
    vf = (mvf * (1000/3600))  # km/h → m/s
    vl = (mvl * (1000/3600))  # km/h → m/s
    al = al
    af = af
    init = (0.5 * (((vf*vf)/abs(af)) - ((vl*vl)/abs(al)))) + (vf * t_r)
    assert(t_r - (vf/af) > (-vl/al))
    return modelmovement(vl, al, vf, af, t_r, init)

# def hg(mvf):
#     """This is perception based warning distance algorithm

#     """
#     vf = (mvf * (1000/3600))  # km/h → m/s
#     init = 0.4905 * vf
#     vl = (51 * (1000/3600))  # km/h → m/s
#     al = af = -5.00
#     assert(t_r - (vf/af) > (-vl/al))
#     t = list(np.arange(0, (t_r - (vf/af)), 1e-2))
#     ret = []
#     for i in t:
#         xl = ((al*((-vl/al)*(-vl/al))/2 + (vl*(-vl/al)))
#               if i > (-vl/al) else (al*(i*i)/2 + (vl*i)))
#         xf = (((af*((i-t_r)*(i-t_r))/2) + (vf*(i-t_r))
#                + (vf * t_r))
#               if i > t_r else (vf * i))
#         crash = xl - xf + (init + (3 * ((xl - xf)/i)))
#         ret.append(crash)
#     return ret


def mazda(mvf, mvl, t_r, al, af):
    t_1 = t_r
    t_2 = 0.6
    t_r = t_1
    d = 5
    vf = (mvf * (1000/3600))  # km/h → m/s
    vl = (mvl * (1000/3600))  # km/h → m/s
    vrel = abs(vf - vl)
    al = al
    af = af
    init = ((0.5 * (((vf*vf)/abs(af)) - ((vl*vl)/abs(al)))) + (vf * t_1)
            + (vrel * t_2) + d)
    assert(t_r - (vf/af) > (-vl/al))
    return modelmovement(vl, al, vf, af, t_r, init)


if __name__ == '__main__':
    plt.style.use('ggplot')
    t_r = 0.0
    mvf = 56
    mvl = 1
    vf = (mvf * (1000/3600))  # km/h → m/s
    al = -5
    af = -1.3
    t = list(np.arange(0, (t_r - (vf/af)), 1e-2))
    dsafe = {'collision': [0]*len(t), 'Berkeley_crash_dist': [],
             'Mazda_crash_dist': [], 'SDA_crash_dist': []}
    dsafe['Berkeley_crash_dist'] = berkeley(mvf, mvl, t_r, al, af)
    dsafe['SDA_crash_dist'] = sda(mvf, mvl, t_r, al, af)
    dsafe['Mazda_crash_dist'] = mazda(mvf, mvl, t_r, al, af)
    # dsafe['HG_crash_dist'] = hg(mvf)
    plt.plot(t, 'Berkeley_crash_dist', data=dsafe, linestyle=':')
    plt.plot(t, 'SDA_crash_dist', data=dsafe, linestyle='-.')
    plt.plot(t, 'Mazda_crash_dist', data=dsafe, linestyle='--')
    # plt.plot(t, 'HG_crash_dist', data=dsafe, linestyle='-')
    plt.plot(t, 'collision', data=dsafe)
    plt.xlabel('Time (sec)')
    plt.ylabel('Distance between LV and FV (m)')
    # plt.title('Follower: 56 km/h, Leader: 51 km/h')
    plt.legend(['Berkeley_vehicle_dist', 'SDA_vehicle_dist',
                'Mazda_vehicle_dist',
                'Collision line'])
    # plt.savefig('./trc_paper/figs/crash.pdf', bbox_inches='tight')
    plt.show()
