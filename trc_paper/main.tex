%% 
%% Copyright 2007-2019 Elsevier Ltd
%% 
%% This file is part of the 'Elsarticle Bundle'.
%% ---------------------------------------------
%% 
%% It may be distributed under the conditions of the LaTeX Project Public
%% License, either version 1.2 of this license or (at your option) any
%% later version.  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.2 or later is part of all distributions of LaTeX
%% version 1999/12/01 or later.
%% 
%% The list of all files belonging to the 'Elsarticle Bundle' is
%% given in the file `manifest.txt'.
%% 
%% Template article for Elsevier's document class `elsarticle'
%% with harvard style bibliographic references

\documentclass[preprint,12pt]{elsarticle}
\synctex=1

%% Use the option review to obtain double line spacing
%% \documentclass[authoryear,preprint,review,12pt]{elsarticle}

%% Use the options 1p,twocolumn; 3p; 3p,twocolumn; 5p; or 5p,twocolumn
%% for a journal layout:
%% \documentclass[final,1p,times,authoryear]{elsarticle}
%% \documentclass[final,1p,times,twocolumn,authoryear]{elsarticle}
%% \documentclass[final,3p,times,authoryear]{elsarticle}
%% \documentclass[final,3p,times,twocolumn,authoryear]{elsarticle}
%% \documentclass[final,5p,times,authoryear]{elsarticle}
%% \documentclass[final,5p,times,twocolumn,authoryear]{elsarticle}

%% For including figures, graphicx.sty has been loaded in
%% elsarticle.cls. If you prefer to use the old commands
%% please give \usepackage{epsfig}

%% The amssymb package provides various useful mathematical symbols
\usepackage{amssymb}
% \usepackage[cache=false]{minted}
\usepackage{listings}
\lstset
{ %Formatting for code in appendix
  language=Python,
  basicstyle=\scriptsize,
  numbers=left,
  stepnumber=1,
  showstringspaces=false,
  frame=single,
  escapeinside={|}{|}
}
\usepackage{xcolor}
\usepackage{subfig}
%% The amsthm package provides extended theorem environments
\usepackage{amsthm}
\usepackage{amsmath}
\newtheorem{lem}{Lemma}
\renewcommand\qedsymbol{$\blacksquare$}
\newproof{prf}{Proof}
\usepackage{tikz} % for drawing
\usetikzlibrary{arrows,shapes,automata,positioning,decorations,calc} % for drawing
\usetikzlibrary{spy,backgrounds}
\pgfdeclaredecoration{complete sines}{initial}
{
	\state{initial}[
	width=+0pt,
	next state=sine,
	persistent precomputation={\pgfmathsetmacro\matchinglength{
			\pgfdecoratedinputsegmentlength / int(\pgfdecoratedinputsegmentlength/\pgfdecorationsegmentlength)}
		\setlength{\pgfdecorationsegmentlength}{\matchinglength pt}
	}] {}
	\state{sine}[width=\pgfdecorationsegmentlength]{
		\pgfpathsine{\pgfpoint{0.25\pgfdecorationsegmentlength}{0.5\pgfdecorationsegmentamplitude}}
		\pgfpathcosine{\pgfpoint{0.25\pgfdecorationsegmentlength}{-0.5\pgfdecorationsegmentamplitude}}
		\pgfpathsine{\pgfpoint{0.25\pgfdecorationsegmentlength}{-0.5\pgfdecorationsegmentamplitude}}
		\pgfpathcosine{\pgfpoint{0.25\pgfdecorationsegmentlength}{0.5\pgfdecorationsegmentamplitude}}
	}
	\state{final}{}
}
% \usepackage[latin1]{inputenc}


\newcommand{\am}[1]{{\textsf{\color{blue} \small{[#1 -am-]}}}}
\newcommand{\jin}[1]{{\textsf{\color{red} \small{[#1 -jin-]}}}}
%% The lineno packages adds line numbers. Start line numbering with
%% \begin{linenumbers}, end it with \end{linenumbers}. Or switch it on
%% for the whole article with \linenumbers.
%% \usepackage{lineno}

\usepackage{acronym}
\usepackage[export]{adjustbox}
\input{acronyms}

\journal{Transportation Research Part B}

\begin{document}

\begin{frontmatter}

%% Title, authors and addresses

%% use the tnoteref command within \title for footnotes;
%% use the tnotetext command for theassociated footnote;
%% use the fnref command within \author or \address for footnotes;
%% use the fntext command for theassociated footnote;
%% use the corref command within \author for corresponding author footnotes;
%% use the cortext command for theassociated footnote;
%% use the ead command for the email address,
%% and the form \ead[url] for the home page:
%% \title{Title\tnoteref{label1}}
%% \tnotetext[label1]{}
%% \author{Name\corref{cor1}\fnref{label2}}
%% \ead{email address}
%% \ead[url]{home page}
%% \fntext[label2]{}
%% \cortext[cor1]{}
%% \address{Address\fnref{label3}}
%% \fntext[label3]{}
%% use optional labels to link authors explicitly to addresses:
%% \author[label1,label2]{}
%% \address[label1]{}
%% \address[label2]{}

  \title{A satisfiability modulo theory formulation to compute a
    collision free minimal longitudinal safety distance\tnoteref{t1}.}
  \tnotetext[t1]{This research was partially funded by University of
    Auckland, Faculty Research and Development Fund (FRDF) grant
    3717133}

  \author{Avinash Malik}
  \ead{avinash.malik@auckland.ac.nz}

  \author{Jin-Woo Ro}
  \ead{jro002@aucklanduni.ac.nz}

  \address{Department of Electrical, Computer and Software
    Engineering,\\ University of Auckland, Auckland, New Zealand}

  \begin{abstract}
    Driver assist technology uses longitudinal safety distance
    algorithms to compute a warning distance to mitigate collisions. A
    number of safety distance algorithms exist. However, none of the
    current safety distance algorithms are accompanied with a proof of
    correctness. This paper proposes for the very first time, a provably
    correct safety distance measure. The proposed safety distance
    formulation, uses \acf{SMT} technology to guarantee the correctness
    of the minimal longitudinal safety distance necessary to
    avoid collision under different driving conditions and the worst
    case braking scenarios. Traffic planners can use the proposed
    framework in conjunction with traffic micro-simulators to determine
    traffic flow, speed limits, and other important traffic planning
    parameters under varying driving conditions.

  \end{abstract}

%%Graphical abstract
% \begin{graphicalabstract}
%\includegraphics{grabs}
% \end{graphicalabstract}

%%Research highlights
% \begin{highlights}
% \item Research highlight 1
% \item Research highlight 2
% \end{highlights}

\begin{keyword}
%% keywords here, in the form: keyword \sep keyword

%% PACS codes here, in the form: \PACS code \sep code

%% MSC codes here, in the form: \MSC code \sep code
%% or \MSC[2008] code \sep code (2000 is the default)
  Longitudinal safety distance measure; warning distance algorithm;
  satisfiability modulo theory
\end{keyword}

\end{frontmatter}

%% \linenumbers

%% main text
\input{intro}
\input{prelim}
\input{formulation}
\input{encoding}
\input{results}
\input{related}
\input{conclusions}

%% The Appendices part is started with the command \appendix;
%% appendix sections are then done as normal sections
%% \appendix

%% \section{}
%% \label{}

%% If you have bibdatabase file and want bibtex to generate the
%% bibitems, please use
%%
\bibliographystyle{elsarticle-harv} 
\bibliography{references}

%% else use the following coding to input the bibitems directly in the
%% TeX file.


\end{document}

\endinput
%%
%% End of file `elsarticle-template-harv.tex'.
