\section{Safety distance algorithm formulation}
\label{sec:formulation}

This section is divided into a number of subsections, for ease of
understanding. We start with stating our assumptions, followed by
derivation of our safety distance formulation.

\subsection{Assumptions}
\label{sec:assumptions}

Our safety distance formulation is under the worst case braking scenario
as shown in Figure~\ref{fig:dynamics1}.

\begin{itemize}
\item (\textbf{A1}) The \ac{LV} and the \ac{FV} have a constant, but
  unknown, initial velocity. Hence,
  $v_{l}(0), v_{f}(0) \in [v^{l}, v^{u}]$, where $v^{l}$, and $v^{u}$ are
  lower and upper bounds of the initial \ac{LV} and \ac{FV} velocity in
  $m/s$.
\item (\textbf{A2}) The \ac{LV} and the \ac{FV} deceleration rates are
  constant, but unknown. Hence, $b_{l}, b_{f} \in [b^{l}, b^{u}]$, where
  $b^{l}$, and $b^{u}$ are the lower and upper bounds on the rate of
  deceleration in $m/s^{2}$.
\item \textbf{(A3)} The \ac{FV} decelerates after some response time
  $\tau$, which is bounded, but unknown. Hence,
  $\tau \in [\tau^{l}, \tau^{u}]$, where $\tau^{l}$, and $\tau^{u}$ are the lower and
  upper bounds on the response time in $s$.
\end{itemize}

\subsection{Deriving the position of the \ac{LV}}
\label{sec:lvposition}

In this section we derive, from first principles, the position of the
\ac{LV}, under the dynamics shows in Figure~\ref{fig:dynamics1}, and the
assumptions stated in Section~\ref{sec:assumptions}. Let $\tau_{lstop}$,
denote the position of the \ac{LV}, when it comes to a stop, i.e.,
$v_{l}(\tau_{lstop}) = 0$. From the constant deceleration rate of the
\ac{LV}, we can derive the position of the \ac{LV} at any time
$t \in [0, \tau_{lstop}]$ as follows:

\begin{align}
  \dot{v_{l}}(t) = -b_{l} \therefore v_{l}(t) = \int-b_{l}\ dt
  \therefore v_{l}(t) = -b_{l}\times t + v_{l}(0) \label{eq:vell} \\
  \dot{x_{l}}(t) = \int(-b_{l} \times t + v_{l}(0))\ dt \nonumber \\
  \therefore x_{l}(t) = \frac{-b_{l}\times t^{2}}{2} + v_{l}(0) \times t + x_{l}(0), \forall t \in [0, \tau_{lstop}] \label{xlt}
\end{align}

\subsection{Deriving the position of the \ac{FV}}
\label{sec:fvposition}

In this section we derive, from first principles, the position of the
\ac{FV}, with the \ac{FV} dynamics as shown in
Figure~\ref{fig:dynamics1}, and the assumptions stated in
Section~\ref{sec:assumptions}. Let $\tau_{fstop}$, be the time when the
\ac{FV} comes to a stop, i.e., $v_{f}(\tau_{fstop}) = 0$. The time
$t \in [0, \tau_{fstop}]$ can be divided into two parts:
$[0, \tau_{fstop}] = [0, \tau] \cup (\tau, \tau_{fstop}]$, from the dynamics of the
\ac{FV} in Figure~\ref{fig:dynamics1}. Hence, we compute the position of
the \ac{FV} for any time $t \in [0, \tau]$, and for any
$t \in (\tau, \tau_{fstop}]$, separately. Using the constant velocity of the
\ac{FV} from time $0$ to $\tau$, we have:

\begin{align}
  \dot{x}_{f}(t) = v_{f}(t) = v_{f}(0) \therefore x(t) = \int v_{f}(0)\ d\tau, \forall t \in [0, \tau] \nonumber \\
  \therefore x_{f}(t) = v_{f}(0)\times t + x_{f}(0), \forall t \in [0, \tau] \label{eq:xft1}
\end{align}

\noindent
Equation~(\ref{eq:xft1}) gives the position of the \ac{FV} at any time
between 0 and $\tau$ seconds, during the human response time. The
derivation for the position of the \ac{FV} from just after the response
time $\tau$ until the \ac{FV} comes to a stop proceeds by first computing
the displacement of the \ac{FV} at any time $t \in (\tau, \tau_{fstop}]$ and
then adding it to the total distance travelled by the \ac{FV} in time
$\tau$, as follows:

\begin{align}
  \mathrm{from\ the\ constant\ deceleration\ of\ the\ \ac{FV}} \nonumber \\
  \dot{v}_{f}(t) = -b_{f} \therefore v_{f}(t) = v_f(\tau) + \int_{\tau}^t -b_{f}\ d\tau, \forall t \in (\tau, \tau_{fstop}] \nonumber \\
  \therefore v_{f}(t) = v_{f}(\tau) - b_{f} (t-\tau), \forall t \in (\tau, \tau_{fstop}] \nonumber \\
  \label{eq:velf} \\
  \dot{x}_{f}(t) = x_f(\tau) + \int_{\tau}^{t} -b_{f} \times (t-\tau) + v_{f}(\tau)\ d\tau, \forall t\in (\tau, \tau_{fstop}] \nonumber \\
  x_{f}(t) = x_f(\tau) + \frac{-b_{f} \times (t - \tau)^{2}}{2} + v_{f}(\tau) \times (t - \tau),
  \forall t \in (\tau, \tau_{fstop}] \nonumber \\
  \because x_f(\tau) = v_{f}(0) \times \tau + x_{f}(0), \mathrm{\ from\ Equation~(\ref{eq:xft1})\ and} \nonumber \\
  \because v_f(\tau) = v_f(0) \mathrm{\ from\ the\ scenario\ dynamics\ in\ Figure~\ref{fig:dynamics1}}  \nonumber \\
  \therefore x_{f}(t) = \frac{-b_{f}\times (t - \tau)^{2}}{2} + v_{f}(0)\times (t - \tau) + v_{f}(0)\times \tau
  + x_{f}(0), \forall t \in (\tau, \tau_{fstop}] \label{eq:xft2}
\end{align}

\subsection{Deriving the stop times for the \ac{LV} and the \ac{FV}}
\label{sec:stoptimes}
% \jin{I changed Equation~\eqref{eq:velf} and~\eqref{eq:xft2}, because there were many mistakes. Note that previously they were: 
% \begin{align}
%   \mathrm{from\ the\ constant\ deceleration\ of\ the\ \ac{FV}} \nonumber \\
%   \dot{v}_{f}(t) = -b_{f} \therefore v_{f}(t) = \int -b_{f} d\tau
%   \therefore v_{f}(t) = -b_{f}\times t + v_{f}(\tau), \forall t \in (\tau, \tau_{fstop}] \nonumber \\
%   \because v_{f}(\tau) = v_{f}(0) \therefore v_{f}(t) = -b_{f}\times t + v_{f}(0), \forall t \in (\tau, \tau_{fstop}]  \\
%   \dot{x}_{f}(t) = \int_{\tau}^{t} (-b_{f} \times t + v_{f}(0)) d\tau, \forall t\in (\tau, \tau_{fstop}] \nonumber \\
%   x_{f}(t) = \frac{-b_{f} \times (t - \tau)^{2}}{2} + v_{f}(0) \times (t - \tau) + C,
%   \forall t \in (\tau, \tau_{fstop}] \nonumber \\
%   \mathrm{C\ is\ the\ distance\ travelled\ by\ the\ \ac{FV}\ at\ time\ \tau}  \nonumber \\
%   \therefore C = v_{f}(0)\times \tau + x_{f}(0), \mathrm{from\ Equation~(\ref{eq:xft1})} \nonumber \\
%   \therefore x_{f}(t) = \frac{-b_{f}\times (t - \tau)^{2}}{2} + v_{f}(0) \times (t - \tau) + v_{f}(0) \times \tau
%   + x_{f}(0), \forall t \in (\tau, \tau_{fstop}] 
% \end{align}
% }

In this section, we derive the stop times, i.e.,
$v_{l}(\tau_{lstop}) = 0$ and $v_{f}(\tau_{fstop}) = 0$, of the \ac{LV} and
the \ac{FV} in terms of initial conditions, which will be necessary to
formulate the safety distance in terms of initial conditions. From
Equation~(\ref{eq:vell}), we have:
\begin{align}
  v_{l}(\tau_{lstop}) = -b_{l}\times \tau_{lstop} + v_{l}(0) 
  \therefore \tau_{lstop} = \frac{v_{l}(0)}{b_{l}} \label{eq:lstoptime}
\end{align}

Similarly, from Equation~(\ref{eq:velf}), we have:
\begin{align}
  v_{f}(\tau_{fstop}) = -b_{f} \times (\tau_{fstop} - \tau) + v_{f}(0), \forall t \in (\tau, \tau_{fstop}]
  \therefore \tau_{fstop} = \tau + \frac{v_{f}(0)}{b_{f}} \label{eq:fstoptime}
\end{align}

\begin{lem}
  The \ac{LV} and the \ac{FV} position formulations
  (Equations~(\ref{xlt}) and~(\ref{eq:xft2})) are the same as
  Equations~(\ref{eq:leaderberkeley}) and~(\ref{eq:berkeleyfollower}),
  at time $\tau_{lstop}$ and $\tau_{fstop}$ used by the Berkeley safety
  distance algorithm.
  \label{lem:1}
\end{lem}
\begin{prf}
  Substituting Equation~(\ref{eq:lstoptime}) into Equation~(\ref{xlt}),
  we have
  $x_{l}(\tau_{lstop}) = \frac{b_{l} \times v^{2}_{l}(0)}{2} + x_{l}(0)$.
  Similarly, substituting Equation~(\ref{eq:fstoptime}) into
  Equation~(\ref{eq:xft2}), we have
  $x_{f}(\tau_{fstop}) = (\frac{v^{2}_{f}(0)}{2\times b_{f}}) + (v_{f}(0)\times\tau) +
  x_{f}(0)$.
  \qed
\end{prf}

\subsection{The safety distance formulation}
\label{sec:safety}
We are finally ready to give the overall safety distance formulation.
The primary requirement is that the two vehicles never collide, i.e.,
the positions of the vehicles should not overlap at \textit{any} given
point in time. This requirement can be formulated as shown in
Equations~(\ref{eq:obj})-(\ref{eq:st5}).

Equation~(\ref{eq:obj}) states that over the entire dynamics of the
worst case breaking scenario (Figure~\ref{fig:dynamics1}); from the
initial time instant until both vehicles come to a stop, the \ac{LV} and
the \ac{FV} never collide, under the constraints given in
Equations~(\ref{eq:st1})---(\ref{eq:st5}). Equation~(\ref{eq:st1}) gives
the position of the \ac{LV}. The position of the \ac{LV} changes
continuously, until it comes to a stop at time $\tau_{lstop}$. Once it
comes to a stop, it remains there for eternity. Similarly, the \ac{FV}
position is divided into three time-lines following
Equation~(\ref{eq:xft1}), Equation~(\ref{eq:xft2}), and once it comes to
a stop at time $\tau_{fstop}$. Equations~(\ref{eq:st3}),~(\ref{eq:st4}),
and~(\ref{eq:st5}) constraint the stop times, and the initial
conditions, according to the assumptions in
Section~\ref{sec:assumptions}.

\begin{align}
  \mathrm{Req}: & x_{l}(t) - L - x_{f}(t) > 0,
         \forall t \in [0, \mathrm{max}(\tau_{lstop}, \tau_{fstop})] \label{eq:obj} \\
  \mathrm{such}\ & \mathrm{that} \nonumber \\
  x_{l}(t) &=
  \begin{cases}
    \frac{-b_{l}\times t^{2}}{2} + v_{l}(0) \times t + x_{l}(0), \forall t \in [0, \tau_{lstop}] \\
    \frac{b_{l} \times v^{2}_{l}(0)}{2} + x_{l}(0), \forall t \in (\tau_{lstop}, \infty)
  \end{cases} \label{eq:st1} \\
  x_{f}(t) &=
   \begin{cases}
     v_{f}(0)\times t + x_{f}(0), \forall t \in [0, \tau] \\
     \frac{-b_{f}\times (t - \tau)^{2}}{2} + v_{f}(0) \times (t - \tau) + v_{f}(0) \times \tau
     + x_{f}(0), \forall t \in (\tau, \tau_{fstop}] \\
     (\frac{v^{2}_{f}(0)}{2\times b_{f}}) + (v_{f}(0)\times\tau) + x_{f}(0), \forall t \in (\tau_{fstop}, \infty)
   \end{cases} \label{eq:st2}\\
  \tau_{lstop} &= \frac{v_{l}(0)}{b_{l}}, 
  \tau_{fstop} = \tau + \frac{v_{f}(0)}{b_{f}} \label{eq:st3} \\
  \tau \in & [\tau^{l}, \tau^{u}], v_{l}(0) \in [v^{l}, v^{u}], v_{f}(0) \in [v^{l}, v^{u}],
    b_{l} \in [b^{l}, b^{u}], b_{f} \in [b^{l}, b^{u}] \label{eq:st4}\\
  \tau^{l}, & \tau^{u}, v^{l}, v^{u}, b^{l}, b^{u} \in \mathbb{R}^{\geq 0} \label{eq:st5}
\end{align}

Equations~(\ref{eq:st1}) and~(\ref{eq:st2}) are piecewise continuous
functions of time. In particular, the position of the vehicle is the
same at the left and right limit of any given discontinuity. For
example, the position of the \ac{LV} and the \ac{FV} are the same just
before, at, and after $\tau_{lstop}$ and $\tau_{fstop}$, respectively, as
given by Lemma~\ref{lem:1}. Similarly, for the discontinuity at $\tau$, we
have
$\lim_{t \to \tau} (\frac{-b_{f}\times (t - \tau)^{2}}{2} + v_{f}(0) \times (t - \tau) +
v_{f}(0) \times \tau + x_{f}(0)) = v_{f}(0) \times \tau + x_f(0)$, from
Equation~(\ref{eq:xft2}), which is the same as the one obtained from
Equation~(\ref{eq:xft1}), by substituting $\tau$ in
Equation~(\ref{eq:xft1}).

The traffic engineer is expected to give the lower and upper bounds for
the response time, the initial velocities, and the deceleration rates.
Our safety distance formulation outputs the \textit{minimum} initial
distance $D(0) = x_{l}(0) - L - x_{f}(0)$, such that the \ac{LV} and the
\ac{FV} never collide. Overall, this is an optimisation problem, where
the Equations~(\ref{eq:obj})---(\ref{eq:st5}) need to be satisfied. We use
a \acf{SMT}~\cite{barrett2009satisfiability} solver to achieve this
optimisation objective.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End: