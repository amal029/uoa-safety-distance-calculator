\section{Experimental results}
\label{sec:results}

In this section we present the quantitative results to understand the
efficacy of the proposed safety distance formulation. All experiments
were carried out on dual core Intel$^{\textregistered}$ Core i7-7600U
CPU running at 2.8 GHz, with 16 GB of RAM, running Linux 4.15 kernel. We
used the Z3~\cite{de2008z3} solver with Python-3.6.8 bindings to carry
out our experiments. Finally, the baseline Berkeley safety distance
algorithm, along with all other safety distance algorithms were also
implemented in Python-3.6.8.

This section presents two sets of results: \textcircled{1} we compare
the \ac{LV} and the \ac{FV} dynamics under the safety distance
calculated by the proposed formulation against the Berkeley safety
distance algorithm for different scenarios in
Section~\ref{sec:coll-scen-comp}. \textcircled{2} We show the impact on
traffic flow using the safety distance computed by the proposed
technique in Section~\ref{sec:vehicular-flow-under}.

\subsection{Collision scenario comparison with Berkeley safety distance
  formulation}
\label{sec:coll-scen-comp}

\begin{figure}[tb]
  \centering
  \subfloat[Scenario, $v_{l}(0) = v_{f}(0) = 51 km/h$, $b_{l} =
  b_{f} = 5 m/s^{2}$, $\tau = 1.2s$, $d_{0}=L=5m$.\label{fig:resultcollision1}]{
    \includegraphics[scale=0.35]{figs/resultnocrash.pdf}
  }
  \qquad
  \subfloat[Scenario, $v_{l}(0) = v_{f}(0) = 51 km/h$, $b_{l} =
  b_{f} = 5 m/s^{2}$, $\tau = 0.25s, 3.0s$, $d_{0}=L=5m$.\label{fig:resultcollisiontr}]{
    \includegraphics[scale=0.35]{figs/resultdifftr.pdf}
  }

  \subfloat[Scenario, $v_{l}(0) = v_{f}(0) = 51 km/h$,
  $b_{l} = 5m/s^{2}, b_{f} = 4.3 m/s^{2}$, $d_{0}=L=5m$,
  $\tau = 1.2s$.\label{fig:resultcollision2}]{
    \includegraphics[scale=0.35]{figs/resultdiffdecel.pdf}
  }
  \qquad
  \subfloat[Scenario, $v_{l}(0) = 51km/h, v_{f}(0) = 56 km/h$,
  $b_{l} = b_{f} = 5.0 m/s^{2}$, $d_{0}=L=5m$,
  $\tau = 1.2s$.\label{fig:resultcollision3}]{
    \includegraphics[scale=0.35]{figs/resultdiffvel.pdf}
  }
  
  \caption{Worst case braking scenarios under varying initial
    conditions}
  \label{fig:resultcollision}
\end{figure}

\begin{figure}[thb]
  \centering
  \subfloat[Scenario, $v_{l}(0) = 51km/h, v_{f}(0) = 51 km/h$,
  $b_{l} = 5.0 m/s^{2}$, $b_{f} \in (5.0, 2.2) m/s^{2}$, $d_{0}=L=5m$,
  $\tau = 1.2s$.\label{fig:resultgeneral2}]{
    \includegraphics[scale=0.35]{figs/diffdecelgeneralreult.pdf}
  }
  \qquad
  \subfloat[Scenario, $v_{l}(0) = 50km/h$, $v_{f}(0) \in (50, 100) km/h$,
  $b_{l} = 5m/s^{2}, b_{f} = 5.0 m/s^{2}$, $d_{0}=L=5m$,
  $\tau = 1.2s$.\label{fig:resultgeneral1}]{
    \includegraphics[scale=0.35]{figs/diffvelgeneralreult.pdf}
  }
  \caption{Collision detection with varying initial velocity and maximum
    deceleration}
  \label{fig:generalresults}
\end{figure}

Figure~\ref{fig:resultcollision} compares the distance between the
\ac{LV} and the \ac{FV} with passage of time with varying initial
conditions, under the worst case braking scenario.
Figure~\ref{fig:resultcollision1} shows that if the \ac{LV} and the
\ac{FV} are moving at the same initial velocity and have the same
maximum deceleration rate, then the Berkeley and the proposed safety
distance formulations compute the exact same safety distance. In fact,
the dynamics under the worst case braking scenario are exactly the same
--- overlapping curves in Figure~\ref{fig:resultcollision1}.
Figure~\ref{fig:resultcollisiontr} shows the dynamics with different
response times for the \ac{FV}. The key fact we infer from
Figure~\ref{fig:resultcollisiontr}, is that both the Berkeley and the
proposed safety distance formulations correctly account for variation in
response times. Increasing response times does not lead to collisions,
as the safety distance increases, see the overlapping
Berkeley\_vehicle\_dist\_tr\_3.0 and Proposed\_vehicle\_dist\_tr\_3.0
curves in Figure~\ref{fig:resultcollisiontr}. Moreover, as expected, a
very small response time, e.g., 0.25 s, accounting for just the braking
dynamics in autonomous cars, reduces the safety distance significantly
in both the formulations.

Figure~\ref{fig:resultcollision2} shows the scenario, where no vehicle
is speeding, however, the \ac{FV}' deceleration rate is 0.7 $m/s^{2}$
less than the \ac{LV}. As we can see in this case, the Berkeley
formulation computes an incorrect safety distance leading to a
collision. On the other hand, the proposed formulation is
\textit{collision free}. The frequency of collision, as the \ac{FV}
deceleration rate reduces, in the general case, is shown in
Figure~\ref{fig:resultgeneral2}. As we can see from
Figure~\ref{fig:resultgeneral2}, the safety distance computed by the
proposed formulation never leads to a collision, even when the
difference between the deceleration rates of the \ac{LV} and the \ac{FV}
differ by 2.8 $m/s^{2}$. The Berkeley formulation, on the other hand,
always leads to collision once the difference is greater than 0.6
$m/s^{2}$.

Figure~\ref{fig:resultcollision3} shows the case where the \ac{FV} is
speeding, 5 $km/h$ faster, than the \ac{LV}, but has the same maximum
deceleration rate as the \ac{LV}. Here again, the Berkeley formulation
computes an incorrect safety distance leading to collision. However, the
proposed formulation is collision free. The general case of the \ac{FV}
speeding, with increasing initial velocity difference between the
\ac{LV} and the \ac{FV} is shown in Figure~\ref{fig:resultgeneral1}. As
we can see, the safety distance computed by the proposed formulation
never leads to a collision, but Berkeley safety distance formulation
always leads to a collision, once the \ac{FV} is 4 $km/h$ faster than
the \ac{LV}.

\subsection{Vehicular flow under different initial conditions}
\label{sec:vehicular-flow-under}

\begin{figure}[tb]
  \centering
  \subfloat[Flow with, $v_{l}(0) = v_{f}(0) = 51 km/h$, $b_{l} =
  b_{f} = 5 m/s^{2}$, $\tau = 1.2s$, $L=5m$.\label{fig:resultflow1}]{
    \includegraphics[scale=0.35]{figs/flow1.pdf}
  }
  \qquad
  \subfloat[Flow with, $v_{l}(0), v_{f}(0) \in (0, v^{u})$, $b_{l}, b_{f}
  \in (1.44, 6.44)  m/s^{2}$, $\tau \in (0.25, 3.0) s$, $L=5m$.\label{fig:resultflow2}]{
    \includegraphics[scale=0.35]{figs/flowvar.pdf}
  }
  \caption{Impact on traffic flow under safety distance computed by the
    proposed technique.}
  \label{fig:flowresults}
\end{figure}

Traffic flow, is the number of vehicles that pass through a given lane
every hour. We compute the traffic flow as:
$\frac{1000 * v}{dsafe + 5}\ vh/hr/lane$, where $1000$ is the lane
length in meter, $v$ is the maximum initial velocity in $km/h$, $dsafe$
is the safety distance computed by the proposed formulation, and $5$ m
is the length of the vehicle.

The traffic flow, under conditions (initial values) similar to Berkeley
and other safety distance algorithms in shown in
Figure~\ref{fig:resultflow1}. As we can see, from
Figure~\ref{fig:resultflow1}, a good traffic flow of around 2600
$vh/hr/lane$ can be achieved at 51 $km/h$. Under these initial values, a
higher traffic flow can be achieved with increasing initial velocity.

The more interesting case, of traffic flow, with varying initial
conditions, where only the proposed formulation can guarantee collision
freedom in shown in Figure~\ref{fig:resultflow2}. In
Figure~\ref{fig:resultflow2}, the \ac{LV}' and the \ac{FV}' initial
velocity can vary anywhere from 0--100 $km/h$, the maximum deceleration
can vary anywhere from 1.44--6.44 $m/s^{2}$, and finally, the response
time for the \ac{FV} can vary from 0.25--3.0 $s$. Under such varying
conditions, the traffic flow is much lower than in
Figure~\ref{fig:resultflow1}. In fact, the highest traffic flow is only
around 650 $vh/hr/lane$ achieved at around 18 $km/h$ speeds. This is as
expected, because $flow \propto \frac{v}{dsafe}$. Under such varying initial
conditions, with increasing speed, a larger safety margin is necessary
to guarantee collision freedom. After a certain maximum initial
velocity, the safety distance starts becoming larger than the initial
velocity and hence, the traffic flow reduces.

Overall, traffic engineers can use the proposed tool in conjunction with
traffic micro-simulators to understand safe driving conditions while
improving traffic flow. The proposed tool takes milli-seconds to output
the optimum safety distance, and is available from~\cite{smttool}.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End: