\section{Introduction}
\label{sec:intro}

Algorithms that warn drivers in the case of increased likelihood of
collision are an important aspect of road safety measures. Longitudinal
collision avoidance (warning) algorithms exist based on: \textcircled{1}
distance measures, such as the popular Berkeley
algorithm~\cite{seiler1998,bella2011}, Mazda algorithm~\cite{doi1994},
\ac{SDA}~\cite{abe2004effect}, and Difference of Space Distance and
Stopping Distance (DSS)~\cite{okamura2011impact}. \textcircled{2}
Perceptual measure such as Hayward' \ac{TTC}~\cite{hayward1972near},
Honda~\cite{bond2003collision}, and Hirst and Graham
algorithm~\cite{hirst1997collision}.
\begin{figure}[t]
  \centering
  % \subfloat[The two car longitudinal setup\label{fig:two-car}]{
    \includegraphics[scale=0.3]{figs/two_car_system.png}
  % }
  \caption{\label{fig:two-car-system} Illustration of the two-car system.}
\end{figure}


\begin{figure}[tbh]
  % \centering
  \subfloat[List of symbols for
  Figure~\ref{fig:two-car-system}\label{tab:symbols}]{
    \begin{scriptsize}
      \begin{tabular}{ | p{39pt} | p{95pt} | }
        \hline
        Symbols		& 	Description \\ 
        \hline
        $v_l(t)$, $v_f(t)$	& Velocity of the LV and the FV in $ms^{-1}$ \\
        $x_l(t)$, $x_f(t)$ & Position of the LV and the FV in  $m$ \\
        $D(t)$	& Vehicle spacing in $m$ ($D(t) = x_l(t) - x_f(t) - L$) \\
        $L$ & Length of the LV in $m$ \\
        $b_l$, $b_f$ & Maximum deceleration of the LV and the FV ($ms^{-2}$)\\ 
        $\tau$ & Response time of the FV in $s$\\
        \hline
      \end{tabular}
    \end{scriptsize}
  }%
  \qquad
  \subfloat[Distance between the \ac{LV} and the \ac{FV} under worst case
  breaking conditions. $\tau=1.2 s$, $b_{l} = b_{f} = 5 m/s^{2}$,
  $v_{l}(0) = 51 km/h, v_{f}(0) = 56 km/h$.\label{fig:crashsub}]{
    \includegraphics[scale=0.5, valign=c]{figs/crash.pdf}
  }
  \caption{The list of symbols used in the rest of the paper and the
    longitudinal distance between vehicles under worst case braking
    scenario.}
  \label{fig:crash}
\end{figure}


Although designed to avoid collision, these algorithms do \textit{not}
prove the correctness of the computed safety distance, and may even lead
to collision under different driving conditions. Consider the scenario
of the \ac{LV} and \ac{FV} as shown in Figure~\ref{fig:two-car-system},
with the annotations, on the figure, described in
Figure~\ref{tab:symbols}. The longitudinal warning distance algorithms
aim to maintain a minimum safety distance between the \ac{LV} and the
\ac{FV} under the worst case driving scenario of the \ac{LV} hard
breaking suddenly, while the \ac{FV} hard breaking after some response
time $\tau$. The \ac{FV} is considered to have collided with the \ac{LV}
under the worst case scenario, if the distance between the two vehicles
($D_{safe}$) goes below zero.

Figure~\ref{fig:crashsub} shows the distance between the \ac{LV} and the
\ac{FV}, with the passage of time, under the worst case driving
scenario, with the initial conditions listed in the figure caption. As
easily visible, the distance between the \ac{LV} and the \ac{FV} goes
below zero with all distance based safety measures,
Berkeley\_crash\_dist, SDA\_crash\_dist, and Mazda\_crash\_dist,
resulting in a crash. Same can be observed for perception based Hirst
and Graham (HG\_vehicle\_dist) warning distance algorithm.

We will use the popular Berkeley safety distance
algorithm~\cite{seiler1998,bella2011} in the rest of the paper to
elucidate the reasons for crash, formulate the problem statement, and
outline the objectives and our contributions.

\subsection{Problem statement and objective}
\label{sec:problem-formulation}

\begin{figure*}[tb]
  \centering
  \subfloat[Dynamics of the \ac{LV} and the \ac{FV} under the worst case
  braking scenario. \label{fig:dynamics1}]{
    \scalebox{0.45}{\input{./figs/dynamics.pdf_t}}
  }
  \qquad
  \subfloat[No collision, $v_{l}(0) = v_{f}(0) = 51 km/h$, $b_{l} =
  b_{f} = 5 m/s^{2}$, $\tau = 1.2s$, $d_{0}=5m$.\label{fig:berkeleysafe}]{
    \includegraphics[scale=0.35]{figs/safe.pdf}
  }

  \subfloat[Collision, $v_{l}(0) = v_{f}(0) = 51 km/h$,
  $b_{l} = 5m/s^{2}, b_{f} = 4.3 m/s^{2}$, $d_{0}=5m$,
  $\tau = 1.2s$.\label{fig:berkeleyaccelcrash}]{
    \includegraphics[scale=0.35]{figs/acceldiffcrash.pdf}
  }
  \qquad
  \subfloat[Collision, $v_{l}(0) = 51km/h, v_{f}(0) = 56 km/h$,
  $b_{l} = b_{f} = 5.0 m/s^{2}$, $d_{0}=5m$,
  $\tau = 1.2s$.\label{fig:berkeleyvelcrash}]{
    \includegraphics[scale=0.35]{figs/veldiffcrash.pdf}
  }
  \caption{Collision scenarios with varying initial conditions under the
    Berkeley warning distance dynamics.}
  \label{fig:dynamics}
\end{figure*}

Most safety distance algorithms, including the Berkeley algorithm
consider the worst case braking dynamics of the \ac{LV} and the \ac{FV}
as shown in Figure~\ref{fig:dynamics1}. The \ac{LV} and the \ac{FV} are
considered to be driving at a maximum constant velocity, until the
\ac{LV} suddenly brakes with a constant deceleration of
$b_{l}\ m/s^{2}$. The position of the \ac{LV}, when it comes to a stop,
at time $\tau_{lstop}$, such that $v_{l}(\tau_{lstop}) = 0$, starting from
some initial position $x_{l}(0)$ is given in
Equation~(\ref{eq:leaderberkeley}). Using the area under the curve in
Figure~\ref{fig:dynamics1}, we can obtain the position of the \ac{LV} at
time $\tau_{lstop}$ as follows:


\begin{align}
  x_{l}(\tau_{lstop}) = \frac{1}{2} \times \tau_{lstop} \times v_{l}(0) + x_{l}(0) \nonumber \\
  \dot{v}_{l}(t) = -b_{l}
  \therefore v_{l}(t) = -b_{l}\times t + v_{l}(0)
  \therefore 0 = -b_{l}(t) \times \tau_{lstop} + v_{l}(0) \nonumber \\
  \therefore x_{l}(\tau_{lstop}) = x_{l}(0) + (\frac{v^{2}_{l}(0)}{2\times b_{l}})\label{eq:leaderberkeley}
\end{align}



The \ac{FV}, on the other hand, responds to the \ac{LV} braking after
some response time $\tau$. During time $\tau$, the \ac{FV} is assumed to be
moving at a constant velocity (see \ac{FV} dynamics in
Figure~\ref{fig:dynamics1}). After $\tau$ seconds, the \ac{FV} hard brakes
at a rate of $b_{f}\ m/s^{2}$, until it comes to a stop. The final
position of the \ac{FV}, given by the area under the curve in
Figure~\ref{fig:dynamics1}, is shown in
Equation~(\ref{eq:berkeleyfollower}), where $x_{f}(0)$ is the initial
position of the \ac{FV}.

\begin{equation}
  \label{eq:berkeleyfollower}
  x_{f}(\tau_{fstop}) = x_{f}(0) + (v_{f}(0)\times\tau) + (\frac{v^{2}_{f}(0)}{2\times b_{f}})
\end{equation}


The safety distance, is the \textit{initial} distance
$D(0) = x_{l}(0) - L - x_{f}(0)$ (see Table~\ref{tab:symbols} for
definition of symbols used), such that the vehicles do not collide, when
they come to a stop i.e.,
$x_{l}(\tau_{lstop}) - L - x_{f}(\tau_{fstop}) > 0$, where $L$ is the length
of the \ac{LV}. Subtracting and rearranging
Equations~(\ref{eq:leaderberkeley}) and~(\ref{eq:berkeleyfollower}),
gives us the Berkeley safety distance algorithm shown in
Equation~(\ref{eq:berkeleyalgorithm}), where $d_{0}$ is extra padding
distance usually set between 1--5 m~\cite{bella2011}.

\begin{equation}
  \label{eq:berkeleyalgorithm}
  D_{safe} = D(0) = \frac{v^{2}_{f}(0)}{2\times b_{f}} - \frac{v^{2}_{l}(0)}{2\times b_{l}}
  + v_{f}(0)\times\tau + d_{0}
\end{equation}

The safety distance, in Equation~(\ref{eq:berkeleyalgorithm}), depends
solely upon the initial conditions.
Figures~\ref{fig:berkeleysafe}---~\ref{fig:berkeleyvelcrash} give the
distance between the \ac{LV} and the \ac{FV}, with the passage of time,
under the worst case braking scenario, with varying initial conditions.
No collision is possible (Figure~\ref{fig:berkeleysafe}), using the
Berkeley safety distance algorithm, if and only if, the \ac{LV} and the
\ac{FV} have the \textit{same} initial velocity and rate of
deceleration. However, slightly changing the rates of deceleration, a
difference of $0.7 m/s^{2}$ (Figure~\ref{fig:berkeleyaccelcrash}) or
initial velocity, a difference of $5 km/hr$
(Figure~\ref{fig:berkeleyvelcrash}) results in a crash. It is well known
that different vehicles have different deceleration
rates~\cite{bokare2017acceleration} and can be moving at different
initial velocities. Overall, the Berkeley safety distance algorithm,
like other safety distance measures, is unsafe.

The overarching objective of the paper is to provide a framework that
\textit{guarantees} collision free safety (warning) distance under
varying deceleration, velocity, and response times during the
aforementioned worst case braking scenario. Our technical
\textbf{contributions} can be listed as follows:

\begin{enumerate}
\item We derive a new formulation for the safety distance measure that
  guarantees collision freedom during worst case longitudinal braking.
\item The safety distance computed by the proposed formulation
  guarantees collision freedom, under varying driving scenarios, by
  construction, using \acf{SMT} solvers.
\item The safety distance formulation can be used to compute, collision
  free, \textit{minimal} safety distance, to improve traffic flow.
\item The developed framework can be used by traffic engineers to model
  traffic flow under varying driving conditions, e.g., dry roads, wet
  roads, varying follower, and leader vehicle braking dynamics, etc.
\end{enumerate}

The rest of the paper is arranged as follows: Section~\ref{sec:prelim}
describes \ac{SMT} solvers. In Section~\ref{sec:formulation} we
formulate our safety distance algorithm. Section~\ref{sec:encoding}
describes the encoding of the safety distance formulation as an \ac{SMT}
satisfiability/optimisation objective. The results of the safety
distance computation, and resultant traffic flow are presented in
Section~\ref{sec:results}. Comparison with current state-of-the-art
techniques is presented in Section~\ref{sec:related}. Finally, we
conclude in Section~\ref{sec:conclu}.




%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
