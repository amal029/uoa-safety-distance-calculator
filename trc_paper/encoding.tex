\section{\ac{SMT} encoding of the safety distance formulation}
\label{sec:encoding}

% For completeness' sake, we first describe the working of a \ac{SMT}
% solver and then describe the encoding of our safety distance formulation
% (Section~\ref{sec:safety}) as an \ac{SMT} instance.

% \input{prelim}

% In this paper we provide the first ever \ac{SMT} formulation for
% determining the minimal safety distance, for following vehicles, which
% is guaranteed to not cause a collision.

We first describe the encoding of the safety distance formulation within
\ac{SMT}, and then describe the minimisation of the safety distance.

\subsection{\ac{SMT} encoding of the safety distance formulation}
\label{sec:smtecoding}

\begin{figure}[t]
  \centering
  \begin{lstlisting}[language=Python]
from z3 import Real, SolverFor, If
def smt_encoding(trmin, trmax, vmin, vmax, bmin, bmax, dsafe_usr):
 # Initialise the needed variables (|$Equation\ (\ref{eq:st5})$|)
 t = Real('t'), bl = Real('bl'), bf = Real('bf'), vl = Real('vl') |$\label{lst:0}$|
 vf = Real('vf'), tr = Real('tr')  # Response time |$\label{lst:1}$|

 # Initialise quantifier free non-linear real arithmetic solver
 solver = SolverFor(logic='QF_NRA') |$\label{lst:2}$|

 # Add constraints (|$Equation\ (\ref{eq:st4})$|)
 solver.add(bl >= bmin, bl <= bmax, bf >= bmin, bf <= bmax, vl >= vmin, |$\label{lst:3}$|
            vl <= vmax, vf >= vmin, vf <= vmax, tr >= trmin, tr <= trmax)
 # Leader and follower position |$(Equations\ (\ref{eq:st1})\ and\ (\ref{eq:st2}))$|
 xl = If((t > (vl/bl)), (vl*vl/2*bl), ((-bl)*(t*t)/2 + (vl*t)))
 xf = If((t <= tr), (vf * t),
      If((t >= tr + (vf/bf)) ((vf*vf)/(2*bf) + (vf * tr)),
      (((-bf) * (t - tr) * (t - tr)/2) + vf * (t - tr) + (vf * tr)))) |\label{lst:4}|

 # The initial spacing between two cars: |$x_{l}(0) - L - x_{f}(0)$|
 dsafe = Real('dsafe') |$\label{lst:6}$|
 # Initialise safety distance to some user given value
 solver.add(dsafe >= dsafe_usr) |$\label{lst:7}$|

 # Model the main safety objective (|$Equation\ (\ref{eq:obj})$|)
 solver.add(((xl - xf + dsafe) <= 0), t >= 0, t <= max(vl/bl, (tr + vf/bf))) |$\label{lst:8}$|
 # Check if the vehicles ever collide
 return solver.check() # Should be unsat, if vehicles don't collide |$\label{lst:11}$|
  \end{lstlisting}
  
  \caption{\ac{SMT} encoding of the safety distance formulation}
  \label{fig:smtencoding}
\end{figure}

Figure~\ref{fig:smtencoding} shows the safety distance formulation
(Equations~(\ref{eq:obj})-(\ref{eq:st5})) encoded as a satisfaction
problem in \ac{SMT}, written in Python bindings to the
Z3~\cite{de2008z3} \ac{SMT} solver. There is an almost one-to-one
correspondence between the safety distance algorithm formulation in
(Equations~(\ref{eq:obj})-(\ref{eq:st5})) and
Figure~\ref{fig:smtencoding}.

The Python function takes as input user defined lower and upper bounds
for the initial conditions and a parameter $dsafe\_usr$, which gives the
initial safety distance between the \ac{LV} and the \ac{FV}, at time
$t=0$. We then start with initialising all the required variables in the
domain of real numbers (Lines~\ref{lst:0}-\ref{lst:1}). Next, we
initialise the \ac{SMT} solver, with the \ac{QFNRA} theory. We choose a
non-linear theory, because our equations are non-linear
(Equations~(\ref{eq:st1}) and~(\ref{eq:st2})). Furthermore, our theory
is quantifier free, i.e., it does not allow the use of the forall
($\forall$) quantifier. Lines~\ref{lst:3}-\ref{lst:4} add the bounds for the
initial conditions, and the position of the \ac{LV} and the \ac{FV},
under the worst case braking scenario, following the formulation given
Section~\ref{sec:safety}.

We then initialise the safety distance between the \ac{LV} and the
\ac{FV} (lines~\ref{lst:6}-\ref{lst:7}), at time $t = 0$, to a user
given value $dsafe\_usr$. Finally, we model the main safety objective
from Equation~(\ref{eq:obj}) on lines~\ref{lst:8}-\ref{lst:11}. The main
safety objective differs from Equation~(\ref{eq:obj}), because
quantifier free theories do not allow encoding forall ($\forall$). The
\textit{key difference} between Equation~(\ref{eq:obj}) and the encoding
on lines~\ref{lst:8}-\ref{lst:11}, is that \textit{instead} of
guaranteeing that the distance between the \ac{LV} and the \ac{FV} is
greater than zero, we guarantee that there \textit{exists} no time $t$,
where the distance between the \ac{LV} and the \ac{FV} is less than or
equal to zero. The encoding uses the equivalence given in
Lemma~\ref{lem:translation}. Hence, the value of $dsafe$, the safety
distance, should be such that the output of the \ac{SMT} solver is
\textbf{unsat} (unsatisfied), for collision freedom.

\begin{lem}
  \label{lem:translation}
  $\forall t \in [0, \mathrm{max}(\tau_{lstop}, \tau_{fstop})], x_{l}(t) - L -x_{f}(t) > 0 \iff \\ \exists t
  \notin [0, \mathrm{max}(\tau_{lstop}, \tau_{fstop})], (x_{l}(t) - L - x_{f}(t) \leq 0)$
\end{lem}


\subsection{Optimising the safety distance}
\label{sec:optimsation}

\begin{figure}[tb]
  \centering
  \begin{lstlisting}[language=Python]
def call_smt_encoding():
 vmin = 0, vmax = 50 # User specified min and max initial velocity (m/s) |$\label{lst2:1}$|
 bmin = 1.44    # User specified min deceleration |$(m/s^{2})$|
 bmax = 6.44    # User specified max deceleration |$(m/s^{2})$|
 trmin = 1.2    # User specified min response time (s)
 trmax = 3.0     # User specified max response time (s)
 dsafe = dprev = 1e-6    # initial safety distance (m) |$\label{lst2:7}$|
 # Call the smt solver very first time
 ret = smt_encoding(trmin, trmax, vmin, vmax, bmin, bmax, dsafe) |$\label{lst2:9}$|
 # Now check if the model is sat or unsat
 while (ret == z3.sat): # sat means collision occurs |$\label{lst2:11}$|
     dprev = dsafe
     dsafe *= 2 # Increase safety distance, double it
     # Call until smt checker guarantees no collision
     ret = smt_encoding(trmin, trmax, vmin, vmax, bmin, bmax, dsafe) |$\label{lst2:15}$|
 # Once collision free, binary search for minimal safety distance
 else: |$\label{lst2:17}$|
     # Now do a binary search between dsafe and dprev
     dsafe = bsearch(dsafe, dprev, vmax, vmin, trmin, trmax, bmin, bmax) |$\label{lst2:19}$|
     return dsafe # return the minimum, collision free, safety distance |$\label{lst2:20}$|
  \end{lstlisting}
  \caption{Code to optimise the safety distance, and guarantee collision
    freedom, under varying initial conditions, via \ac{SMT}}
  \label{fig:optimisesafetydist}
\end{figure}

Figure~\ref{fig:optimisesafetydist} gives the top-level that calls the
smt\_encoding procedure (Figure~\ref{fig:smtencoding}) and optimises the
safety distance $dsafe$. The function first initialises the lower and
upper bounds for the initial conditions
(lines~\ref{lst2:1}-\ref{lst2:7}). In particular, note that the safety
distance is initialised to a very small value on line~\ref{lst2:7}.
Next, the \ac{SMT} encoding in Figure~\ref{fig:smtencoding} is called,
which gives back a \textbf{sat} (satisfied) or \textbf{unsat}
(unsatisfied) value. A \textbf{sat} value indicates \textit{collision},
an \textbf{unsat} indicates \textit{collision freedom}. The top-level
function keeps on doubling the safety distance ($dsafe$) until an
\textbf{unsat} is returned (lines~\ref{lst2:11}-\ref{lst2:15}). Finally,
once collision freedom is detected, the function performs a binary
search (line~\ref{lst2:19}), between previous and current safety
distance, to return the minimal $dsafe$ that guarantees collision
freedom (line~\ref{lst2:20}).


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
