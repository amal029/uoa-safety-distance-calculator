import matplotlib.pyplot as plt
import numpy as np
from mine import call_main


def berkeley(t_r, af):
    d = 5
    init = (0.5 * (((vf*vf)/af) - ((vl*vl)/al))) + (vf * t_r) + d
    assert(t_r - (vf/af) > (-vl/al))
    ret = []
    for i in t:
        xl = ((al*((-vl/al)*(-vl/al))/2 + (vl*(-vl/al)))
              if i > (-vl/al) else (al*(i*i)/2 + (vl*i)))
        xf = (((af*((i-t_r)*(i-t_r))/2) + (vf*(i-t_r))
               + (vf * t_r))
              if i > t_r else (vf * i))
        crash = xl - xf + init
        ret.append(crash)
    return 1 if ret[-1] <= 0 else 0


def mine(t_r, init, af):
    assert(t_r - (vf/af) > (-vl/al))
    ret = []
    for i in t:
        xl = ((al*((-vl/al)*(-vl/al))/2 + (vl*(-vl/al)))
              if i > (-vl/al) else (al*(i*i)/2 + (vl*i)))
        xf = (((af*((i-t_r)*(i-t_r))/2) + (vf*(i-t_r))
               + (vf * t_r))
              if i > t_r else (vf * i))
        crash = xl - xf + init
        ret.append(crash)
    return 1 if ret[-1] <= 0 else 0


t_r = 1.2
al = -5
# af = -5.0                       # Same velocity but follower stops slower
vf = (51.0 * (1000/3600))  # km/h → m/s
vl = (51 * (1000/3600))  # km/h → m/s


def gen(af=-5.0):
    while af < -2:
        yield af
        af += 0.2


if __name__ == '__main__':
    plt.style.use('ggplot')
    dsafe = {'v': [], 'Berkeley_crash': [],
             'Proposed_crash': []}
    vfk = vf * (3600/1000)
    vlk = vl * (3600/1000)
    (d, _, _, _, _) = call_main(vmin=vlk, vmax=vfk)
    print('init: %s' % d)
    for af in list(gen()):
        t = list(np.arange(0, (t_r - (vf/af)), 1e-2))
        dsafe['v'].append(af - al)
        dsafe['Berkeley_crash'].append(berkeley(t_r, af))
        dsafe['Proposed_crash'].append(mine(t_r, d, af))
    # print(dsafe)
    plt.scatter(dsafe['v'], 'Berkeley_crash', data=dsafe, marker='*')
    plt.scatter(dsafe['v'], 'Proposed_crash', data=dsafe, marker='+')
    plt.xlabel('Difference between max deceleration (LV - FV) (m/s/s)')
    plt.ylabel('Collision?')
    plt.legend(['Berkeley', 'Proposed'])
    plt.yticks([0, 1], ['No', 'Yes'])
    plt.xticks(dsafe['v'])
    plt.savefig('./trc_paper/figs/diffdecelgeneralreult.pdf',
                bbox_inches='tight')
    plt.show()
